package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pedido_plato")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PedidoPlatoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)// que se genere el Id automaticamente
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)//muchos a uno
    @JoinColumn(name = "id_pedido", nullable = false)
    private PedidoEntity pedido;//tabla pedido

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_plato", nullable = false)
    private PlatoEntity plato;//tabla plato

    private int cantidad;

}
