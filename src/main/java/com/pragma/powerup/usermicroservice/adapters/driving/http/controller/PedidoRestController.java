package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/pedido")
@RequiredArgsConstructor
public class PedidoRestController {

    @Autowired
    private IPedidoHandler pedidoHandler;



    @PostMapping
    public ResponseEntity<String> realizarPedido(@RequestBody PedidoRequestDTO pedidoDTO,String mail,String password) {

        try {
            pedidoHandler.realizarPedido(pedidoDTO,mail,password);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok("The order was created sucesfully and is in state Pending");
    }

    @GetMapping
    public ResponseEntity<Page<PedidoEntity>> obtenerPedidosPorEstado(
            @RequestParam("estado") String estado,
            @RequestParam("elementosPorPagina") int elementosPorPagina,
            @RequestParam("numeroPagina") int numeroPagina ,String mail,String password) throws Exception{

        Page<PedidoEntity> pedidos = pedidoHandler.obtenerPedidosPorEstado(estado, elementosPorPagina, numeroPagina, mail, password);
        return ResponseEntity.ok(pedidos);
    }

    @PutMapping("/cambiarEstadoPreparacion")
    public ResponseEntity<String> cambiarEstadoPedido(@RequestParam int idPedido,
                                                      @RequestParam String mail,
                                                      @RequestParam String password,
                                                      @RequestParam String idEmpleado)

    {
        try {
            pedidoHandler.cambiarEstadoPedido(idPedido, mail, password, idEmpleado);
            return ResponseEntity.ok("El estado del pedido ha sido actualizado a 'Preparacion'.");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/listo")
    public ResponseEntity<String> notificarPedidoListo(@RequestParam String idPedido,
                                                       @RequestParam String mail,
                                                       @RequestParam String password)
    {
        String response = pedidoHandler.notificarPedidoListo(idPedido, mail, password);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{idPedido}/entregar/{pinSeguridad}")
    public void marcarPedidoEntregado(@PathVariable String idPedido, @PathVariable String pinSeguridad ,String mail ,String Password,  String idEmpleado) {
        pedidoHandler.marcarPedidoEntregado(idPedido, pinSeguridad,mail,Password, idEmpleado);
    }

    @PostMapping("/{idPedido}/cancelar")
    public void cancelarPedido(@PathVariable Long idPedido,String mail ,String Password) {
        pedidoHandler.cancelarPedido(idPedido, mail , Password);
    }

}
