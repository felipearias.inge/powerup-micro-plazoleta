package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.usermicroservice.domain.repositories.IPlatoRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;


@Service
@RequiredArgsConstructor
public class PlatoHandlerImpl implements IPlatoHandler {


    private final IPlatoRepository platoRepository;


    public void savePlato(PlatoRequestDto platoRequestDto) throws Exception {

        HttpClient client = HttpClient.newHttpClient();
        String requestBody = "{\"mail\":\"" + platoRequestDto.getMail() + "\", \"password\":\"" + platoRequestDto.getPassword() + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());//Respuesta 200, 400, 500
        String responseBody = response.body();

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();


        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));

        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);
        String role = jwtResponseDto.getRoles().get(0);

            System.out.println("Description: " + role);
            if (role.equals("ROLE_OWNER")) {

                RestaurantEntity restaurante = new RestaurantEntity(platoRequestDto.getIdRestaurante(), "", "", "", "", "", 0);
                //Se debe saber de donde proviene el restaurante
                PlatoEntity platoEntity = new PlatoEntity();
                platoEntity.setNombre(platoRequestDto.getNombre());
                platoEntity.setPrecio(platoRequestDto.getPrecio());
                platoEntity.setDescripcion(platoRequestDto.getDescripcion());
                platoEntity.setUrlImagen(platoRequestDto.getUrlImagen());
                platoEntity.setCategoria(platoRequestDto.getCategoria());
                platoEntity.setActivo(platoRequestDto.isActive());
                platoEntity.setRestaurante(restaurante);


                platoRepository.save(platoEntity);
            } else {
                throw new Exception("The role to assign the dish is not an owner");
            }


    }

    @Override
    public void updatePlatos(Long id, String mail, String password, int precio, String descripcion) {
        HttpClient client = HttpClient.newHttpClient();
        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();


        HttpResponse<String> response2 = null;
        try {
            response2 = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String responseBody = response2.body();


        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        System.out.println("Description: " + role);
        if(role.equals("ROLE_OWNER")){
            PlatoEntity plato = platoRepository.findById(id.toString());
            plato.setPrecio(precio);
            plato.setDescripcion(descripcion);
            platoRepository.save(plato);
        }
    }

    @Override
    public void activationPlato(Long id, String mail, String password, boolean activo) {

        HttpClient client = HttpClient.newHttpClient();
        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();


        HttpResponse<String> response2 = null;
        try {
            response2 = client.send(request, HttpResponse.BodyHandlers.ofString());//200, 400, 500
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String responseBody = response2.body();


        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        System.out.println("Description: " + role);
        if(role.equals("ROLE_OWNER")) {
            if (activo) {
                Optional<PlatoEntity> platoOptional = platoRepository.findById(id);

                if (platoOptional.isPresent()) {
                    PlatoEntity plato = platoOptional.get();
                    plato.setActivo(true);//por defecto lo activa
                    platoRepository.save(plato);
                    System.out.println("Row of table 'plate' with id "+ id + " activated.");
                } else {
                    System.out.println("No row found in table 'dish' with id " + id + ".");
                }
            } else {
                Optional<PlatoEntity> platoOptional = platoRepository.findById(id);

                if (platoOptional.isPresent() && activo==false) {
                    PlatoEntity plato = platoOptional.get();
                    plato.setActivo(false);
                    platoRepository.save(plato);
                    System.out.println("Row of table 'plate' with id " + id + " disabled.");
                }


            }

        }

    }





        public List<PlatoEntity> getPlatosPorCategoria(@PathVariable String restauranteId, String mail, String password) throws Exception {

            HttpClient client = HttpClient.newHttpClient();
            String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("http://localhost:8080/auth/login"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();


            HttpResponse<String> response2 = null;
            try {
                response2 = client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            String responseBody = response2.body();


            Gson gson = new Gson();
            JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
            String token = responseObject.getToken();

            String[] chunks = token.split("\\.");
            Base64.Decoder decoder = Base64.getUrlDecoder();

            String payload = new String(decoder.decode(chunks[1]));
            JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

            String role = jwtResponseDto.getRoles().get(0);

            System.out.println("Description: " + role);
            if(!role.equals("ROLE_CUSTOMER")) {

                throw new Exception("The user is not a customer, try again");
                }
            return platoRepository.getPlatosPorCategoria(restauranteId);

        }



}



