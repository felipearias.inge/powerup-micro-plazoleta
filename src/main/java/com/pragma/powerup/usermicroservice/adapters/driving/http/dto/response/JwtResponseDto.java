package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import java.util.List;

public class JwtResponseDto {

    private List<String> roles;
    public List<String> getRoles() {return roles;}

    public void setRoles (List<String> roles) {this.roles = roles;}

    private String token;

    public JwtResponseDto() {
    }

    public JwtResponseDto(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
