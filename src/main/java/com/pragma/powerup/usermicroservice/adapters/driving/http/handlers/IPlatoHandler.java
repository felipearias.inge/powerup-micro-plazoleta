package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;

import java.util.List;

public interface IPlatoHandler {
    void savePlato(PlatoRequestDto platoRequestDto) throws Exception;

    void updatePlatos(Long id, String mail, String password, int precio, String descripcion);

    void activationPlato (Long id, String mail, String password, boolean activo);

    List<PlatoEntity> getPlatosPorCategoria(String restauranteId, String mail, String pasword) throws Exception;
}
