package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PedidoRequestDTO {
    @NotBlank
    private Long idCliente;
    private Date fecha;
    private String estado;
    private Long idRestaurante;
    //mail y password
    private List<PedidoClientRequestDTO> platos;

}
