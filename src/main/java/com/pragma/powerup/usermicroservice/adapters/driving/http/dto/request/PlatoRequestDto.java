package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PlatoRequestDto {

    @NotBlank
    private String mail;
    @NotBlank
    private String password;
    @NotBlank
    private String nombre;
    private int precio;
    private String descripcion;
    private String urlImagen;
    private String categoria;
    private boolean active;
    private long idRestaurante;

}
