package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDTO;
import org.springframework.data.domain.Page;

public interface IPedidoHandler {

    void realizarPedido(PedidoRequestDTO pedidoDTO, String mail, String password) throws Exception;

    void cambiarEstadoPedido(int idPedido, String mail, String password, String idEmpleado ) throws Exception;//asignar pedido
    Page<PedidoEntity> obtenerPedidosPorEstado(String estado, int elementosPorPagina, int numeroPagina, String mail, String password) throws Exception;


     String notificarPedidoListo(String idPedido, String mail, String password);

    void marcarPedidoEntregado(String idPedido, String pinSeguridad,String mail ,String Password,  String idEmpleado);

    void cancelarPedido(Long idPedido,String mail ,String password);
}


