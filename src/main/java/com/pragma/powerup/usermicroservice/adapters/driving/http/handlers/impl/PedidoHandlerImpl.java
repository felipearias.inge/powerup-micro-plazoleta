package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoClientRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import com.pragma.powerup.usermicroservice.domain.repositories.IPedidoPlatoRepository;
import com.pragma.powerup.usermicroservice.domain.repositories.IPedidoRepository;
import com.pragma.powerup.usermicroservice.domain.repositories.IPlatoRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class PedidoHandlerImpl implements IPedidoHandler {

    PedidoEntity pedidos = new PedidoEntity();

    public String obtenerIdEmpleado(String mail) {

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/user/" + mail + "/id"))
                .GET()
                .build();

        HttpResponse<String> ownerResponse;
        try {
            ownerResponse = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        String idEmpleado = ownerResponse.body();

        return idEmpleado;
    }

    public void generateTrazabilidad(Long idPedido, String idCliente, String correoCliente, String estadoAnterior, String estadoNuevo, Long idEmpleado, String correoEmpleado) {
        String requestBody = "{\"idPedido\": " + idPedido + ", \"idCliente\": \"" + idCliente + "\", \"correoCliente\": \"" + correoCliente + "\", \"estadoAnterior\": \"" + estadoAnterior + "\", \"estadoNuevo\": \"" + estadoNuevo + "\", \"idEmpleado\": " + idEmpleado + ", \"correoEmpleado\": \"" + correoEmpleado + "\"}";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8091/trazabilidad"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response;

        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        String responseBody = response.body();
    }

    public String getRole(String mail, String password) {
        HttpClient client = HttpClient.newHttpClient();

        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        String responseBody = response.body();

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        return role;
    }

    public static final String ACCOUNT_SID = "ACff989d5a073c11a7178f0eb1f14f688d";
    public static final String AUTH_TOKEN = "7547498f7d471f1ad0211fd69f6aa53f";

    @Autowired
    private IPedidoRepository pedidoRepository;
    @Autowired
    private IPedidoPlatoRepository pedidoPlatoRepository;

    @Autowired
    private IPlatoRepository iPlatoRepository;
    public void realizarPedido(PedidoRequestDTO pedidoDTO, String mail, String password) throws Exception {


        HttpClient client = HttpClient.newHttpClient();
        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();


        HttpResponse<String> response2 = null;
        try {
            response2 = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String responseBody = response2.body();


        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        System.out.println("Description: " + role);
        if(!role.equals("ROLE_CUSTOMER")) {

            throw new Exception("The user is not a customer, try again");
        }



        Random random = new Random();


        int numeroAleatorio = random.nextInt(9000) + 1000;
        System.out.println(numeroAleatorio);

        PedidoEntity pedido = new PedidoEntity();
        pedido.setIdCliente(pedidoDTO.getIdCliente());
        pedido.setFecha(new Date());
        pedido.setEstado("Pendiente");


        RestaurantEntity restaurant = new RestaurantEntity();
        restaurant.setId(pedidoDTO.getIdRestaurante());
        pedido.setPing(String.valueOf(numeroAleatorio));
        pedido.setRestaurante(restaurant);

        pedido = pedidoRepository.save(pedido);

        List<PedidoClientRequestDTO> platos =pedidoDTO.getPlatos();
        for (PedidoClientRequestDTO pedidoCliente: platos) {
            PedidoPlatoEntity pedidoPlato = new PedidoPlatoEntity();
            PlatoEntity plato = iPlatoRepository.findById(String.valueOf(pedidoCliente.getIdPlato()));
            if(!plato.isActivo()){
                throw new Exception("The dish wit the id: "+pedidoCliente.getIdPlato()+" is not active" );
            }

            pedidoPlato.setPedido(pedido);
            pedidoPlato.setPlato(plato);
            pedidoPlato.setCantidad(pedidoCliente.getCantidad());
            //**************************************
            generateTrazabilidad(pedido.getId(), String.valueOf(pedido.getIdCliente()), mail, String.valueOf(pedido), "Pendiente", null, null);
            System.out.println(pedidos.getId());

            pedidoPlatoRepository.save(pedidoPlato);
        }//for

     }

    @Override
    public Page<PedidoEntity> obtenerPedidosPorEstado(String estado, int elementosPorPagina, int numeroPagina, String mail, String password) throws Exception {

        HttpClient client = HttpClient.newHttpClient();
        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();


        HttpResponse<String> response2 = null;
        try {
            response2 = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String responseBody = response2.body();


        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        System.out.println("Description: " + role);
        if (!role.equals("ROLE_EMPLOYEE")) {
            throw new Exception("THE ROLE IS NOT A CUSTOMER PLEASE TRY AGAIN");

        }  Pageable pageable = PageRequest.of(numeroPagina, elementosPorPagina);
        return pedidoRepository.findByEstado(estado, pageable);
    }

    @Override
    @Transactional
    public void cambiarEstadoPedido(int idPedido, String mail, String password, String idEmpleado) throws Exception {
        String estadoPedido = pedidoRepository.getEstadoById(String.valueOf(idPedido));
        PedidoEntity pedido = pedidoRepository.findById(idPedido);
        String role = getRole(mail, password);
        System.out.println("Description: " + role);

        if (!role.equals("ROLE_EMPLOYEE")) {
            throw new Exception("The user is not an employee. Please try a different user.");
        }
        if (estadoPedido.equals("en preparacion")) {
            throw new Exception("The order is already in preparation");
        }

        generateTrazabilidad( pedido.getId(), pedido.getIdCliente().toString(), null, estadoPedido, " en preparacion", Long.valueOf(idEmpleado), mail);

        pedidoRepository.pedidoPreparado((long) idPedido);//estado en preparacion
        //pedidoRepository.insertarIdEmpleado(Long.valueOf(idEmpleado), (long) idPedido);

    }


    @Transactional
    public String notificarPedidoListo(String idPedido, String mail , String Password) {
        String idClient = pedidoRepository.findIdClienteById(Long.valueOf(idPedido));
        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/user/phone/" + idClient))
                .GET()
                .build();


        HttpResponse<String> ownerResponse;
        try {
            ownerResponse = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
            return idClient;
        }
        String telefono = ownerResponse.body();

        String estadoanterior = pedidoRepository.getEstadoById(idPedido);
        System.out.println("El estado del era " + estadoanterior);

        pedidoRepository.notificarPedidoListo(Long.valueOf(idPedido));//Listo



        String estadoPedido = pedidoRepository.getEstadoById(idPedido);
        System.out.println("El estado del pedido es " + estadoPedido);
        String pin = pedidoRepository.findPingById(Long.valueOf(idPedido));


        if(estadoPedido.equalsIgnoreCase("Listo")){
            String role = getRole(mail, Password);//metodo

            if (!role.equals("ROLE_EMPLOYEE")) {
                throw new RuntimeException("Unauthorized access. This role can't perform this action.");

            }

        }else {
            throw new RuntimeException("Su pedido no esta listo");


        }
        String idEmpleado = obtenerIdEmpleado(mail);

        generateTrazabilidad(Long.valueOf(idPedido),idClient , null, "Pendiente", "Listo", Long.valueOf(idEmpleado), mail);


        return telefono + "/"+ pin;
    }


    @Transactional
    public void marcarPedidoEntregado(String idPedido, String pinSeguridad,String mail ,String Password, String idEmpleado) {
        String estadoPedido = pedidoRepository.getEstadoById(idPedido);
        PedidoEntity pedido = pedidoRepository.findById(Long.parseLong(idPedido));

        String role = getRole(mail, Password);

        if (!role.equals("ROLE_EMPLOYEE")) {
            throw new RuntimeException("Unauthorized access. This role can't perform this action.");

        }

        System.out.println("El estado del pedido es " + estadoPedido);
        if (estadoPedido != null && estadoPedido.equalsIgnoreCase("Listo")) {
            String pingCliente = pedidoRepository.findPingById(Long.valueOf(idPedido));

            if (pinSeguridad.equals(pingCliente)) {
                generateTrazabilidad(Long.valueOf(idPedido),  String.valueOf(pedido.getIdCliente()), null, estadoPedido, "Entregado", null,mail);
                pedidoRepository.marcarPedidoEntregado(Long.valueOf(idPedido));
                System.out.println("Pedido marcado como entregado.");
            } else {
                throw new RuntimeException("Pin de seguridad incorrecto.");
            }
        } else {
            throw new RuntimeException("El pedido no está en estado 'Listo'.");
        }
    }

   @Transactional
    public void cancelarPedido(Long idPedido,String mail ,String password) {
       String estadoPedido = pedidoRepository.getEstadoById(String.valueOf(idPedido));
       PedidoEntity pedido = pedidoRepository.findById((long) idPedido);

       //el llamado


       String role = getRole(mail, password);

       if (!role.equals("ROLE_CUSTOMER")) {
           throw new RuntimeException("Unauthorized access. This role can't perform this action.");

       }
       long id1 = pedido.getIdCliente();
       long id2 = Long.parseLong(obtenerIdEmpleado(mail));
       System.out.println(pedido.getIdCliente());
       System.out.println(obtenerIdEmpleado(mail));


       if(id1==id2){//validar cliente
           if (estadoPedido != null && estadoPedido.equalsIgnoreCase("Pendiente")) {
               generateTrazabilidad(pedido.getId(),  String.valueOf(pedido.getIdCliente()), null, estadoPedido, "Cancelar", null, null);


               pedidoRepository.cancelarPedido(idPedido);
               System.out.println("Order canceled.");
           } else {
               throw new RuntimeException("Lo sentimos, tu pedido ya está en preparación y no puede cancelarse o .");
           }
       }else {throw new RuntimeException("Lo sentimos, tu autenticacion no es la correcta.");}

    }



}
