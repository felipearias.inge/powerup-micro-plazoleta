package com.pragma.powerup.usermicroservice.adapters.driving.http.test;

import com.pragma.powerup.usermicroservice.adapters.driving.http.controller.PlatoRestController;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PlatoControllerTest {
/*
    @InjectMocks
    private PlatoRestController platoRestController;

    @Mock
    private IPlatoHandler platoHandler;

    @Test
    public void testSavePlato() {
        PlatoRequestDto platoRequestDTO = new PlatoRequestDto(
                "arroz",
                10,
                "arroz con pollo ",
                "image_url",
                "Category",
                1L
        );

        ResponseEntity<Map<String, String>> response = platoRestController.savePlato(platoRequestDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.DISH_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));

    }

    @Test
    public void testUpdatePlato(){
        Long platoId = 1L;
        PlatoRequestDto platoRequestDto = new PlatoRequestDto(

                "Update plato",
                10000,
                "Plato Update",
                "URL.img.UPdate",
                "Update Category",
                1L
        );

        ResponseEntity<String> response = platoRestController.updatePlato(platoId, platoRequestDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Plato actualizado exitosamente", response.getBody());
    }*/
}
