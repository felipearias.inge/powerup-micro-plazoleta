package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestaurantRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestaurantResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestaurantHandler;
import com.pragma.powerup.usermicroservice.domain.repositories.IRestaurantRepository;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RestaurantHandlerImpl implements IRestaurantHandler {
    @Autowired
    private final IRestaurantRepository restaurantRepository;


    public void saveRestaurant(RestaurantRequestDto restaurantRequestDto) throws Exception {

        HttpClient client = HttpClient.newHttpClient();
        String requestBody = "{\"mail\":\"" + restaurantRequestDto.getMail() + "\", \"password\":\"" + restaurantRequestDto.getPassword() + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();


        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String responseBody = response.body();


        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));//se lecciona la parte donde esta el rol
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        if (role.equals("ROLE_ADMIN")) {

            HttpRequest requestOwner = HttpRequest.newBuilder()
                    .uri(URI.create("http://localhost:8080/user/" + restaurantRequestDto.getIdPropietario()))
                    .header("Authorization", "Bearer " + token)
                    .GET()
                    .build();
            HttpResponse<String> ownerResponse = client.send(requestOwner, HttpResponse.BodyHandlers.ofString());
            String ownerResponseBody = ownerResponse.body();

            JSONObject jsonObject = new JSONObject(ownerResponseBody);

            if (ownerResponseBody.contains("error")) {
                throw new Exception("The user doesn't exist");

            }

            String description = jsonObject.getJSONObject("idRole").getString("description");


            if (description.equals("ROLE_OWNER")) {

                RestaurantEntity restaurantEntity = new RestaurantEntity();
                restaurantEntity.setName(restaurantRequestDto.getName());
                restaurantEntity.setNit(restaurantRequestDto.getNit());
                restaurantEntity.setAddress(restaurantRequestDto.getAddress());
                restaurantEntity.setPhone(restaurantRequestDto.getPhone());
                restaurantEntity.setUrlLogo(restaurantRequestDto.getUrlLogo());
                restaurantEntity.setIdPropietario(restaurantRequestDto.getIdPropietario());

                restaurantRepository.save(restaurantEntity);
            } else {
                throw new Exception("The user to assign the restaurant is not an owner");
            }
        } else {
            throw new Exception("This user dont have authorization to create a restaurant");
        }



    }


@ApiResponse
            public List<RestaurantResponseDto> getRestaurantData(String mail,String password)  throws Exception {
                HttpClient client = HttpClient.newHttpClient();
                String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password+ "\"}";

                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("http://localhost:8080/auth/login"))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                        .build();
                HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
                String responseBody = response.body();//200, 400, 500


                Gson gson = new Gson();
                JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
                String token = responseObject.getToken();

                String[] chunks = token.split("\\.");
                Base64.Decoder decoder = Base64.getUrlDecoder();

                String payload = new String(decoder.decode(chunks[1]));
                JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

                String role = jwtResponseDto.getRoles().get(0);

                List<RestaurantEntity> restaurants = restaurantRepository.getRestaurantData();
                List<RestaurantResponseDto> restaurantResponseDtos = new ArrayList<>();//crea una lista array

                if (role.equals("ROLE_CUSTOMER")){
                    for (RestaurantEntity restaurantRequest :restaurants ) {

                        RestaurantResponseDto restaurantResponse = new RestaurantResponseDto();
                        restaurantResponse.setName(restaurantRequest.getName());
                        restaurantResponse.setUrlLogo(restaurantRequest.getUrlLogo());
                        restaurantResponseDtos.add(restaurantResponse);

                    }
                } else {
                    throw new Exception("Role is not allowed");
                }

                    return restaurantResponseDtos;

        }
}
