package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.ActivationPlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.IdPlatoRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/plato")
@RequiredArgsConstructor

public class PlatoRestController {
    private final IPlatoHandler iplatoHandler;



    @Operation(summary = "Add a new plato",
            responses = {
                    @ApiResponse(responseCode = "201", description = "plato created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))
            })
    @PostMapping

    public ResponseEntity<Map<String, String>> savePlato(@RequestBody PlatoRequestDto platoRequestDto) {
        try {
            iplatoHandler.savePlato(platoRequestDto);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.DISH_CREATED_MESSAGE));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updatePlatos(@PathVariable("id") Long id, @RequestBody IdPlatoRequestDTO idPlatoRequestDTO) {   try {
        iplatoHandler.updatePlatos(id,idPlatoRequestDTO.getMail(),idPlatoRequestDTO.getPassword(), idPlatoRequestDTO.getPrecio(), idPlatoRequestDTO.getDescripcion());
        return ResponseEntity.ok("Dish successfully upgraded");
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
    }

    @PostMapping("activatePlatos/{id}")
    public ResponseEntity<String> activatePlatos(@PathVariable("id") Long id, @RequestBody ActivationPlatoRequestDto activationPlatoRequestDTO) {
        try {
            iplatoHandler.activationPlato(id, activationPlatoRequestDTO.getMail(), activationPlatoRequestDTO.getPassword(), activationPlatoRequestDTO.isActivo());
            return ResponseEntity.ok("Dish successfully upgraded");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/restaurante/{restauranteId}")
    public ResponseEntity<List<PlatoEntity>> getPlatosPorCategoria(@PathVariable("restauranteId") String restauranteId, String mail, String password) throws Exception{
        List<PlatoEntity> platos = iplatoHandler.getPlatosPorCategoria(restauranteId, mail, password);
        return ResponseEntity.ok(platos);
    }

}

