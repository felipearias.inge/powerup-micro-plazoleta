package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IPedidoRepository extends JpaRepository<PedidoEntity, Long> {

    Page<PedidoEntity> findByEstado(String estado, Pageable pageable);


    PedidoEntity findById(long idPedido);

    @Query("SELECT p.estado FROM PedidoEntity p where p.id= ?1")
    String getEstadoById(String idPedido);


    @Modifying
    @Query("UPDATE PedidoEntity p SET p.estado = 'Entregado' WHERE p.id = ?1")
    void marcarPedidoEntregado(Long idPedido);

    @Modifying
    @Query("UPDATE PedidoEntity p SET p.estado = 'Cancelado' WHERE p.id = ?1")
    void cancelarPedido(Long idPedido);

    @Query("SELECT p.ping FROM PedidoEntity p WHERE p.id = ?1")
    String findPingById(Long idPedido);

    @Query("SELECT p.idCliente FROM PedidoEntity p WHERE p.id = ?1")
    String findIdClienteById(Long idPedido);

    @Modifying
    @Query("UPDATE PedidoEntity p SET p.estado = 'en preparacion' WHERE p.id = ?1")
    void pedidoPreparado(Long idPedido);

   /* @Modifying
    @Query("UPDATE PedidoEntity p SET p.idChef = idEmpleado WHERE p.id = idPedido")
    void insertarIdEmpleado(Long idEmpleado, Long idPedido);*/


    @Modifying
    @Query("UPDATE PedidoEntity p SET p.estado = 'Listo' WHERE p.id = ?1")
    void notificarPedidoListo(Long idPedido);




}

