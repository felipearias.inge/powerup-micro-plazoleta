package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IPlatoRepository extends JpaRepository<PlatoEntity, Long> {

    PlatoEntity findById(String id);

    @Query("SELECT p FROM PlatoEntity p WHERE p.restaurante.id = :restaurantId " +
            "AND p.categoria IN (SELECT DISTINCT p2.categoria FROM PlatoEntity p2 WHERE p2.restaurante.id = :restaurantId)")
    List<PlatoEntity> getPlatosPorCategoria(@Param("restaurantId") String restauranteId);




}
