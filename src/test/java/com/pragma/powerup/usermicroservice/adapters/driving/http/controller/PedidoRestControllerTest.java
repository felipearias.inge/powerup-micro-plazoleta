package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


class PedidoRestControllerTest {


    @InjectMocks
    private PedidoRestController pedidoRestController;

    @Mock
    private IPedidoHandler pedidoHandlerImpl;



    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void realizarPedido() throws Exception {
        PedidoRequestDTO pedidoDTO = new PedidoRequestDTO(1L,new Date(),"preferente",2L,new ArrayList<>());
        pedidoDTO.setIdCliente(1L);
        pedidoDTO.setFecha(new Date());
        pedidoDTO.setEstado("Pending");
        pedidoDTO.setIdRestaurante(3L);
        pedidoDTO.setPlatos(new ArrayList<>());
        String mail = "test@example.com";
        String password = "password";

        ResponseEntity<String> response = pedidoRestController.realizarPedido(pedidoDTO, mail, password);

        assertEquals("The order was created sucesfully and is in state Pending", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void obtenerPedidosPorEstado() throws Exception {

        // Arrange
        String estado = "Pending";
        int elementosPorPagina = 10;
        int numeroPagina = 1;
        String mail = "test@example.com";
        String password = "password";

        Page<PedidoEntity> pedidosMock = mock(Page.class);
        when(pedidoHandlerImpl.obtenerPedidosPorEstado(estado,elementosPorPagina, numeroPagina, mail, password))
                .thenReturn(pedidosMock);

        ResponseEntity<Page<PedidoEntity>> response = pedidoRestController.obtenerPedidosPorEstado(estado, elementosPorPagina, numeroPagina, mail, password);

        assertEquals(pedidosMock, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void cambiarEstadoPedido() throws Exception{
        int idPedido = 1;
        PedidoRequestDTO estadoPedidoDTO = new PedidoRequestDTO(1L,new Date(),"Preparacion",2L, new ArrayList<>());
        estadoPedidoDTO.setIdCliente(1L);
        estadoPedidoDTO.setFecha(new Date());
        estadoPedidoDTO.setEstado("Shipped");
        estadoPedidoDTO.setIdRestaurante(3L);
        estadoPedidoDTO.setPlatos(new ArrayList<>());
        String mail = "test@example.com";
        String password = "password";

        ResponseEntity<String> response = pedidoRestController.cambiarEstadoPedido(idPedido, mail, password, "");

        assertEquals("Order status changed successfully.", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }



    @Test
    public void testNotificarPedidoListo() {
        String idClient = "123";
        String mail = "test@example.com";
        String password = "password";
        String expectedResponse = "Pedido listo";

        when(pedidoHandlerImpl.notificarPedidoListo(idClient, mail, password)).thenReturn(expectedResponse);

        String response = pedidoHandlerImpl.notificarPedidoListo(idClient, mail, password);


        assertEquals(expectedResponse, response);
    }
    @Test
    public void testMarcarPedidoEntregado() {
        String idPedido = "1";
        String pinSeguridad = "1234";
        String mail = "mail@example.com";
        String password = "password";
        doNothing().when(pedidoHandlerImpl).marcarPedidoEntregado(idPedido, pinSeguridad, mail, password, "");
        pedidoRestController.marcarPedidoEntregado(idPedido, pinSeguridad, mail, password, "");
        verify(pedidoHandlerImpl).marcarPedidoEntregado(idPedido, pinSeguridad, mail, password, "");
    }

    @Test
    public void testCancelarPedido() {
        Long idPedido = 1L;
        String mail = "mail@example.com";
        String password = "password";
        doNothing().when(pedidoHandlerImpl).cancelarPedido(idPedido, mail, password);
        pedidoRestController.cancelarPedido(idPedido, mail, password);
        verify(pedidoHandlerImpl).cancelarPedido(idPedido, mail, password);
    }

}