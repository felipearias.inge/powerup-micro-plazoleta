package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestaurantRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestaurantHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl.RestaurantHandlerImpl;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class RestaurantRestControllerTest {

    @InjectMocks
    private RestaurantRestController restaurantRestController;

    @Mock
    private IRestaurantHandler restaurantHandler;



    @BeforeEach
    void setUp(){

        restaurantHandler = mock(RestaurantHandlerImpl.class);
        restaurantRestController =  new RestaurantRestController(restaurantHandler);
    }
    @Test
    public void testSaveRestaurant() throws Exception {


        RestaurantRequestDto restaurantRequestDto = new RestaurantRequestDto(
                "2",
                "321",
                "",
                "",
                "",
                "",
                "",
                1
        );


        ResponseEntity<Map<String, String>> response = restaurantRestController.saveRestaurant(restaurantRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.RESTAURANT_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));

    }
}