package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.ActivationPlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.IdPlatoRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl.PlatoHandlerImpl;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PlatoRestControllerTest {

    @InjectMocks
    private PlatoRestController platoRestController;


    @Mock
    private IPlatoHandler platoHandlerImpl;


    @BeforeEach
    void setUp(){

        platoHandlerImpl = mock(PlatoHandlerImpl.class);
        platoRestController =  new PlatoRestController(platoHandlerImpl);
    }


    @Test
    public void testSavePlato() throws Exception {

     PlatoRequestDto platoRequestDTO = new PlatoRequestDto("","","",10,"","","", true, 1L);

        doNothing().when(platoHandlerImpl).savePlato(platoRequestDTO);

        ResponseEntity<Map<String, String>> response = platoRestController.savePlato(platoRequestDTO);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.DISH_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));
    }

    @Test
    public void testUpdatePlato(){
        Long platoId = 1L;
        PlatoRequestDto platoRequestDto = new PlatoRequestDto(

                "Update plato",
                "asdad",
                "Plato Update",
                1000,
                "Update Category",
                "",
                "",
                true,
                1L
        );

        ResponseEntity<String> response = platoRestController.updatePlatos(platoId,new IdPlatoRequestDTO(
                "",
                "",
                1L,
                100,
                ""));

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Plato actualizado exitosamente", response.getBody());
    }

    @Test
    public void testActivatePlatos() {
        Long id = 1L;
        ActivationPlatoRequestDto activationPlatoRequestDTO = new ActivationPlatoRequestDto("example@mail.com","password",true);

        ResponseEntity<String> response=platoRestController.activatePlatos(id, activationPlatoRequestDTO);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

    }

    @Test
    void testGetPlatosPorCategoria() throws Exception {

        String restauranteId = "1";
        String mail = "test@example.com";
        String password = "password";
        List<PlatoEntity> platosMock = Arrays.asList(new PlatoEntity(), new PlatoEntity());

        when(platoHandlerImpl.getPlatosPorCategoria(anyString(), anyString(), anyString()))
                .thenReturn(platosMock);

        ResponseEntity<List<PlatoEntity>> response = platoRestController.getPlatosPorCategoria(restauranteId, mail, password);

        assertEquals(platosMock, response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }
}